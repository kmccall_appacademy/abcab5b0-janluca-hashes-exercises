# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  hash = Hash.new
  str.split.each do  |word|
    hash[word] = word.length
  end
  hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  arr = hash.sort_by { |k, v| v }
  arr[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each_key do |k|
    older[k] = newer[k]
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  hash = Hash.new(0)
  word.chars.each do |letter|
    hash[letter] += 1
  end
  hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = Hash.new
  arr.each do |ele|
    hash[ele] = 0
  end
  hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hash = { even: 0, odd: 0 }
  numbers.each do |number|
    if number % 2 == 0
      hash[:even] += 1
    else
      hash[:odd] += 1
    end
  end
  hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel = ['a', 'e', 'i', 'o', 'u']
  hash = Hash.new(0)
  string.chars.each do |letter|
    if vowel.include?(letter)
      hash[letter] += 1
    end
  end
  sorted = hash.sort_by { |k,v| v }
  sorted[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  later_student_pairs = []
  later_students_names = sort_to_student_name(students)
  idx = 0
  later_students_names.length.times do
    inner_idx = idx + 1
    until inner_idx == later_students_names.length
      first_student = later_students_names[idx]
      second_student = later_students_names[inner_idx]
      if first_student != second_student
        later_student_pairs << [first_student, second_student]
      end
      inner_idx += 1
    end
    idx += 1
  end
  later_student_pairs
end

def sort_to_student_name(arr)
  student_names = []
  arr.each do |k, v|
    if v >= 7
      student_names << k
    end
  end
  student_names
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species = count_species_to_h(specimens)
  number_of_species = species.length
  sorted_species = species.sort_by { |k,v| v }
  largest_population_size = sorted_species[-1][-1]
  smallest_population_size = sorted_species[0][-1]

  number_of_species**2 * smallest_population_size / largest_population_size
end

def count_species_to_h(arr)
  hash = Hash.new(0)
  arr.each do |animal|
    hash[animal] += 1
  end
  hash
end
# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_hash = character_count(normal_sign)
  vandal_hash = character_count(vandalized_sign)

  vandal_hash.each_key do |key|
    if vandal_hash[key] > normal_hash[key]
      return false
    end
  end
  true
end

def character_count(str)
  hash = Hash.new(0)
  str.chars.each do |letter|
    if letter != " "
      hash[letter.downcase] += 1
    end
  end
  hash
end
